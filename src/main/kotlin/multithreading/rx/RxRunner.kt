package multithreading.rx

import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.functions.Action
import io.reactivex.rxjava3.functions.Consumer

fun main() {

    val names = Observable.just("Borya", "Max", "Lena")
    names.subscribe(::println) //println - это потребитель событий, а names - источник событий

    val numbers = listOf(4,15,63,23,3)
    Observable.fromIterable(numbers)
        .map { 2*it }
        .subscribe(::println)

    val stringConsumer = Consumer<String>{ println(it)}
    val errorConsumer = Consumer<Throwable>{ println(it)}
    val endAction = Action{ println("ended")}

    val subscription = names.subscribe(
        stringConsumer,
        errorConsumer,
        endAction
    )

    Observable.fromCallable { callApi() }.subscribe(::println)

}

fun callApi(): String{
    Thread.sleep(200)
    return "call api"
}