package multithreading

import java.util.concurrent.Callable
import java.util.concurrent.Executors
import java.util.concurrent.Future

fun main() {

    val service = Executors.newFixedThreadPool(2)
    service.submit(MyRunnable())
    service.submit(MyRunnable())
    service.submit(MyRunnable())
    service.submit(MyRunnable())
    service.submit(MyRunnable())
    service.submit(MyRunnable())

    val result: Future<String> = service.submit(MyCallable())

    if (result.isDone){
        println(result.get()) //попадём на эту ветку только если поток выполнился и результат есть
    }

    println(result.get()) //вызвав так мы блокируем выполнение текущего потока до того момента, когда будет получен результат


    service.shutdown() //ждёт когда все потоки выполнятся
//    service.shutdownNow() //завершится стразу же

}


class MyRunnable: Runnable{
    override fun run() {
        Thread.sleep(200)
        println("thread id is: ${Thread.currentThread().id}")
    }
}

class MyCallable: Callable<String>{
    override fun call(): String {
        Thread.sleep(200)
        return "callable thread id is: ${Thread.currentThread().id}"
    }
}