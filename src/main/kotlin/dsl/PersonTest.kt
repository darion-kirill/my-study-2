package dsl

data class Person(var name: String, var age: Int){

    var nicknames = mutableListOf<String>()

    operator fun String.unaryPlus(){
        nicknames.add(this)
    }

    override fun toString(): String {
        return "Person $name $age $nicknames"
    }
}

fun person(block: Person.() -> Unit): Person = Person("", 0).apply(block)

fun main() {

    val max = person {
        name = "Max"
        age = 56
        + "Maximus"
    }

    println(max)

}